#coding:utf-8
from django.http import HttpResponse
from resolution import *
import logging
import json
import time
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

def index(request):
    stime = time.time()
    url, description = param.get_desc_and_url(request.GET['url'])
    logging.info('url: %s' % url)
    response_result = get_response(url)
    if response_result['status'] == status.SUCCESS:
        logging.info('SUCCESS! It takes %f seconds.' % (time.time()-stime))
    else:
        logging.warning('FAILED! It takes %f seconds. Return code is %d' % (time.time()-stime, response_result['status']))
    return HttpResponse(json.dumps(response_result,ensure_ascii=False,indent=2),content_type="application/json;charset=utf-8")

def get_response(url):
    if 'b.mashort.cn' in url:
        response_result = taobao.get_response(url)
    elif '/m.jd.com' in url or '.m.jd.com' in url or 'item.jd.com' in url:
        response_result = jd.get_response(url)
    elif 'amazon.cn' in url:
        response_result = amazon.get_response(url)
    else:
        response_result = result.get_result(status=status.URL_ERROR)
    return response_result
