# coding:utf-8
import re
import urllib2
import status
import result
import socket
import logging
from urllib import unquote
from bs4 import BeautifulSoup

id_pattern1 = re.compile('url=http://a.m.taobao.com/i(\d+).htm')
id_pattern2 = re.compile('[\?\&]id=(\d+)')
price_pattern1 = re.compile('[\?\&]price=([\d\.]+)')
prefix = 'http://hws.m.taobao.com/cache/wdetail/5.0/?id='
send_headers = {
    'user_agent':'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8G4 Safari/6533.18.5'
}

def set_proxy(http_proxy, https_proxy):
    proxy_handler = urllib2.ProxyHandler(
        {'https': https_proxy, 'http': http_proxy})
    opener = urllib2.build_opener(proxy_handler)
    urllib2.install_opener(opener)

def get_response(url, proxy=False):
    try:
        if proxy:
            set_proxy('115.227.192.117:3128','115.227.192.117:3128')
        req = urllib2.Request(url, headers=send_headers)
        page = urllib2.urlopen(req,timeout=status.timeout).read()
        soup = BeautifulSoup(page, 'html.parser')
    except socket.timeout,e:
        logging.error(e)
        return result.get_result(status=status.TIMEOUT_ERROR)
    except urllib2.HTTPError,e:
        if e.code==404:
            logging.error(e)
            return result.get_result(status=status.URL_ERROR)
        else:
            logging.error(e)
            return result.get_result(status=status.SERVICE_ERROR)
    except Exception,e:
        logging.error(e)
        return result.get_result(status=status.SERVICE_ERROR)
    idcontent = unquote(soup.find(id='J_Url')['value'])
    if not idcontent:
        logging.error('taobao idcontent not found!')
        return result.get_result(status=status.URL_ERROR)
    match1 = id_pattern1.search(idcontent)
    if match1:
        i = match1.group(1)
    else:
        match2 = id_pattern2.search(idcontent)
        if match2:
            i = match2.group(1)
        else:
            logging.error('taobao id not found!')
            return result.get_result(status=status.URL_ERROR)
    match3 = price_pattern1.search(idcontent)
    if match3:
        price = match3.group(1)
    else:
        logging.error('taobao price not found')
        return result.get_result(status=status.URL_ERROR)
    url = prefix+i
    try:
        req = urllib2.Request(url, headers=send_headers)
        content = eval(urllib2.urlopen(req,timeout=status.timeout).read().replace('true','True').replace('false','False'))
        desc = content['data']['itemInfoModel']['title']
        picsPath = content['data']['itemInfoModel']['picsPath']
    except socket.timeout,e:
        logging.error(e)
        return result.get_result(status=status.TIMEOUT_ERROR)
    except Exception,e:
        logging.error(e)
        return result.get_result(status=status.URL_ERROR)
    pictures = []
    for each in picsPath:
        pictures.append({'low':each+'_60x60.jpg', 'high':each})
    return result.get_result(status=status.SUCCESS, desc=desc, pictures=pictures, price={'price':price})