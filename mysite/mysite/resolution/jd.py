#coding:utf-8
from bs4 import BeautifulSoup
import urllib2
import result
import status
import re
import socket
import logging

id_pattern1 = re.compile('/(\d+)\.')
prefix = 'http://item.m.jd.com/ware/view.action?wareId='
send_headers = {
	'user_agent':'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8G4 Safari/6533.18.5'
}

def set_proxy(http_proxy, https_proxy):
	proxy_handler = urllib2.ProxyHandler(
		{'https': https_proxy, 'http': http_proxy})
	opener = urllib2.build_opener(proxy_handler)
	urllib2.install_opener(opener)

def get_response(url, proxy=False):
	match1 = id_pattern1.search(url)
	if match1:
		i = match1.group(1)
	else:
		logging.error('jd id not found!')
		return result.get_result(status=status.URL_ERROR)
	url = prefix+i
	try:
		if proxy:
			set_proxy('115.227.192.117:3128','115.227.192.117:3128')
		req = urllib2.Request(url, headers=send_headers)
		page = urllib2.urlopen(req, timeout=status.timeout).read()
		soup = BeautifulSoup(page, 'html.parser')
	except socket.timeout,e:
		logging.error(e)
		return result.get_result(status=status.TIMEOUT_ERROR)
	except urllib2.HTTPError,e:
		if e.code==404:
			logging.error(e)
			return result.get_result(status=status.URL_ERROR)
		else:
			logging.error(e)
			return result.get_result(status=status.SERVICE_ERROR)
	except Exception,e:
		logging.error(e)
		return result.get_result(status=status.SERVICE_ERROR)
	price = get_price(soup)
	desc = get_desc(soup)
	pictures = get_pictures(soup)
	if not price:
		logging.error('price not found!')
		return result.get_result(status=status.URL_ERROR)
	if not pictures:
		logging.error('picture not found!')
		return result.get_result(status=status.URL_ERROR)
	if not desc:
		logging.error('description not found!')
		return result.get_result(status=status.URL_ERROR)
	return result.get_result(status=status.SUCCESS, desc=desc, pictures=pictures, price=price)

def get_price(content):
	res = {}
	price = content.find(id='jdPrice')
	if price:
		res = {'price':price['value']}
	return res

def get_desc(content):
	desc = content.find(id='goodName')
	if desc:
		return desc['value']
	return ''

def get_pictures(content):
	res = []
	pictures = content.find(id='imgs')
	if pictures:
		for each in pictures['value'].split(','):
			if each:
				res.append({'high':each, 'low':each.replace('/n12/','/n5/')})
	return res
