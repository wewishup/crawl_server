#coding:utf-8

def get_result(status, desc='', pictures=[], price={}, form={}):
	return {
		'status':status,
		'description':desc,
		'picture':pictures,
		'price':price,
		'form':form
	}