# coding:utf-8
import re

url_pattern = re.compile('(http\S+?)(\s|\(|$)')

def get_desc_and_url(param):
	url, desc = '', ''
	match = url_pattern.search(param)
	if match:
		url = match.group().strip()
		desc = param[:match.start()].strip().encode('utf-8')
		desc = desc.replace('淘宝商品：','').replace('我在京东发现了一个不错的商品：','').replace('，点击查看：','')
	return url, desc

