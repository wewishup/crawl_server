#coding:utf-8
from bs4 import BeautifulSoup
import urllib2
import result
import status
import re
import socket
import logging

desc_pattern1 = re.compile("title(?:'|\") content=\"([\s\S]+?)\"")
price_pattern1 = re.compile("div id=('|\")price('|\")[\s\S]+?</div>")
price_pattern2 = re.compile("<tr[\s\S]+?>(\S+?)</td>\s*<td[\s\S]+?>([^<>]+?)</(td|span)>".decode('utf-8'))
picture_pattern1 = re.compile("colorImages('|\"):([\s\S]+?\}\]\})")
send_headers = {
    'user_agent':'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8G4 Safari/6533.18.5'
}

def set_proxy(http_proxy, https_proxy):
    proxy_handler = urllib2.ProxyHandler(
        {'https': https_proxy, 'http': http_proxy})
    opener = urllib2.build_opener(proxy_handler)
    urllib2.install_opener(opener)

def get_response(url, proxy=False):
    try:
        if proxy:
            set_proxy('115.227.192.117:3128','115.227.192.117:3128')
    except Exception,e:
        logging.error(e)
        return result.get_result(status=status.SERVICE_ERROR)
    count = 0
    while(True):
        try:
            req = urllib2.Request(url, headers=send_headers)
            page = urllib2.urlopen(req, timeout=status.timeout).read()
            break
        except socket.timeout,e:
            logging.error(e)
            return result.get_result(status=status.TIMEOUT_ERROR)
        except urllib2.HTTPError,e:
            if e.code==503:
                count += 1
                logging.warning(e)
                if count >= 10:
                    logging.error('two many times of service 503!')
                    return result.get_result(status=status.TIMEOUT_ERROR)
            elif e.code==404:
                logging.error(e)
                return result.get_result(status=status.URL_ERROR)
            else:
                logging.error(e)
                return result.get_result(status=status.SERVICE_ERROR)
        except Exception,e:
            logging.error(e)
            return result.get_result(status=status.SERVICE_ERROR)
    price = get_price(page)
    pictures = get_pictures(page)
    desc = get_desc(page)
    if not price:
        logging.error('price not found!')
        return result.get_result(status=status.URL_ERROR)
    if not pictures:
        logging.error('picture not found!')
        return result.get_result(status=status.URL_ERROR)
    if not desc:
        logging.error('description not found!')
        return result.get_result(status=status.URL_ERROR)
    return result.get_result(status=status.SUCCESS, desc=desc, pictures=pictures, price=price)

def get_price(content):
    res = {}
    match1 = price_pattern1.search(content)
    if match1:
        match2 = price_pattern2.findall(match1.group())
        if match2:
            for each in match2:
                tmp1 = each[0].encode('utf-8')
                if tmp1[-1] == '：' or tmp1[-1] == ':':
                    tmp1 = tmp1[:-1]
                res[tmp1] = ''.join(each[1].encode('utf-8').replace(',','').replace('￥','').split())
    return res

def get_pictures(content):
    res = []
    match1 = picture_pattern1.search(content)
    if match1:
        pictures = eval(match1.group(2).replace("null", "''"))
        for each in pictures["initial"]:
            res.append({"low":each["thumb"], "high":each["large"]})
    return res

def get_desc(content):
    match = desc_pattern1.search(content)
    if match:
        return match.group(1).strip().encode('utf-8')
    else:
        return ''
