#环境配置

* python 2.7
* Django 1.8
* beautifulsoup4
* urllib
* urllib2

#启动方式

* 本地django启动：进入mysite，命令行输入
```
python manage.py runserver
```
* 外网django启动：进入mysite，命令行输入
```
python manage.py runserver 182.92.216.189:8000
```
* 服务器apache启动
```
service httpd start
```


#请求方式

* 本地访问模式：进入浏览器，输入
```
localhost:8000/index?url=请求的url
```
* 外网访问模式：进入浏览器，输入
```
http://182.92.216.189:8000/index?url=请求的url
```
> 注意：请求的url可以是商品的以http开头的链接，也可以是手机淘宝或手机亚马逊复制的内容，即商品描述+商品链接。**必须经过urlencode**。

#返回格式

```
{
	'status':200,
	'picture':[
		{
			'high':(高清图片url)
			'low':(低清图片url)
		},
		{
			'high':
			'low':
		}
	],
	'price':{
		'价格':x,
		'促销价':y,
		'京东价':z
	},
	'description':'',
	'form':{}
}
```
* status表示返回状态，正常返回为**200**，发生超时异常（5秒内没能得到页面）返回**110**，url错误（即url不是有效的京东/天猫/亚马逊链接）返回**120**，服务器错误返回**500**。
* picture为图片对应的高清和低清url链接
* 目前没有返回表单

#终止方式

* django启动时，在命令行输入ctrl+C
* apache启动时，在命令行输入service httpd stop

